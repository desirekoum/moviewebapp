Here we create an application that will manage the tasks of a movie theater. In this application will be created the functionalities that will satisfy the requirements by performing the following tasks.

list, add, edit, delete a movie
list, add, edit, delete more than one movie
list movies by year, category, rating
store movies by category
add, edit, delete categories
login page
store in database
use online movies (API)
add security token to convert to the API
