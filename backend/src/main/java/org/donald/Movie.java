package org.donald;

/**
 * @author DONALD KEITA
        *
        */
public class Movie {

    private String title;
    private String length;
    private Rating rating;
    private String category;
    private String year;

    /**
     * @param title
     * @param length
     * @param rating
     * @param category
     */
    public Movie(String title, String length, Rating rating, String category, String year) {
        //super();
        this.title = title;
        this.length = length;
        this.rating = rating;
        this.category = category;
        this.year = year;
    }

    /**
     * @return the title
     */
    public String getTitle() {
        return title;
    }
    /**
     * @param title the title to set
     */
    public void setTitle(String title) {
        this.title = title;
    }
    /**
     * @return the time
     */
    public String getLength() {
        return length;
    }
    /**
     * @param time the time to set
     */
    public void setLength(String length) {
        this.length = length;
    }
    /**
     * @return the rating
     */
    public Rating getRating() {
        return rating;
    }
    /**
     * @param rating the rating to set
     */
    public void setRating(Rating rating) {
        this.rating = rating;
    }
    /**
     * @return the category
     */
    public String getCategory() {
        return category;
    }
    /**
     * @param category the category to set
     */
    public void setCategory(String category) {
        this.category = category;
    }

    public String getYear() {
        return year;
    }

    public void setYear(String year) {
        this.year = year;
    }

    @Override
    public String toString() {
        return "Movie [title=" + title + ", length=" + length + ", rating=" + rating + ", category=" + category
                + ", year=" + year + "]";
    }


}
