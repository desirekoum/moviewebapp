package org.donald;

/**
 * @Author: desir Mpondo on Jan, 2022
 */
enum Rating {

    General_Audiences("G"),
    Parental_control_Guidance("PG"),
    Parents_Strongly_Cautioned("PG-13"),
    Restricted("R"),
    Clearly_Adult("NC-17");

    private final String value;

    Rating(String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }
}

