package org.donald;
/**
 * @author DONALD KEITA
 *
 */

//import java.util.*;
import java.util.ArrayList; // import the ArrayList class
import java.util.Scanner;  // Import the Scanner class
import java.lang.String; // import the String class

public class Main {

    /**
     * @param args
     */
    public static void main(String[] args) {
        // TODO Auto-generated method stub

        //Create a Scanner object
        Scanner input = new Scanner(System.in);
        System.out.println("Movie Theater - BEST MOVIES");
        System.out.println("Choose one action: Add, Edit,delete, or Display a movie");
        System.out.println("");
        //An array of actions to perform
        String[] actions = {"ADD", "EDIT", "DELETE", "DISPLAY"};
        //declaring local variables
        String title, rating, catgName, length, year;
        //String action;
        char again;

        //Create MovieManager object
        MovieManager manager = new MovieManager();

        //do-while loop to give option to user to repeat an action or perform another one
        do {

            //choose an action to perform
            System.out.println("Enter an action");
            String action = input.nextLine();
            action = action.toUpperCase();

            //checking if input is valid
            while(check(actions, action) != true) {
                System.out.println("Wrong input: enter a valid action");
                action = input.nextLine();
                action = action.toUpperCase();
            }
            System.out.println("");
            switch(action) {
                case "ADD":
                    manager.add();
                    break;
                case "EDIT":
                    manager.edit();
                    break;
                case "DELETE":
                    manager.delete();
                    break;
                default:
                    manager.displayMovies();
            }
            System.out.println("");
            //choose an action to perform
            System.out.print("Would you like to enter an action?  Yes or No:  ");
            again = input.nextLine().toUpperCase().charAt(0);
            //validate input char
            while(again != 'Y' && again != 'N') {
                //choose an action to perform
                System.out.print("Wrong input character! Please carefully enter Y or N : " );
                again = input.nextLine().toUpperCase().charAt(0);
            }
            System.out.println("");
        }
        while (again == 'Y');

        //display the list of the movie categories
        manager.displayCategories();
    }

    //************************************************************
    //************************************************************
    //************************************************************

    /**
     * @param arr - an array of type String
     * @param element - element of type String
     * @return true or false
     */
    public static boolean check(String[] arr, String element) {
        boolean isIn = false;
        for (String current : arr) {
            if (current.equals(element) == true) {
                isIn = true;
                break;
            }
        }
        return isIn;
    }

    //************************************************************
    //************************************************************
    //************************************************************
}
