package org.donald;
import java.util.ArrayList;
import java.util.Scanner;

/**
 * @author DONALD KEITA
 */
public class MovieManager {


    //ArrayList of type Movie
    private ArrayList<Movie> movieList;
    //ArrayList of type Category
    private ArrayList<Category> categoryList;
    //Create a Scanner object
    Scanner input = new Scanner(System.in);



    /**
     * default constructor - No-argument constructor
     */
    public MovieManager() {
        this.movieList = new ArrayList<Movie>();
        this.categoryList = new ArrayList<Category>();
    }

    /**
     * @return the movieList
     */
    public ArrayList<Movie> getMovieList() {
        return movieList;
    }

    /**
     * @param movieList the movieList to set
     */
    public void setMovieList(ArrayList<Movie> movieList) {
        this.movieList = movieList;
    }

    /**
     * void() function with no argument
     * the function adds a movie in the movie list
     * The function also adds a category in the category list
     */
    public void add() {

        //An array of movie ratings to choose
        String[] ratings = {"G", "PG", "PG-13", "R", "NC-17"};
        //declaring local variables
        String title, rating, catgName, length, year;

        //The movie title
        System.out.print("Enter the movie title ==> ");
        title = input.nextLine();
        title = title.toUpperCase();
        //The movie length
        System.out.print("Enter the movie length ==> ");
        length = input.nextLine();
        //The movie rating
        System.out.println("G / PG / PG-13 / R / NC-17");
        System.out.print("Enter the movie rating ==> ");
        rating = input.nextLine();
        rating = rating.toUpperCase();
        //checking if input is valid
        while(checkElement(ratings, rating) == false) {
            System.out.println("Wrong input: enter a valid rating");
            rating = input.nextLine();
        }
        //
        Rating movingRating;
        if (rating == "G")
            movingRating  = Rating.General_Audiences;
        else if (rating == "PG")
            movingRating = Rating.Parental_control_Guidance;
        else if (rating == "PG-13")
            movingRating = Rating.Parents_Strongly_Cautioned;
        else if (rating == "R")
            movingRating = Rating.Restricted;
        else
            movingRating = Rating.Clearly_Adult;
        //The movie category
        System.out.print("Enter the movie category ==> ");
        catgName = input.nextLine();
        //The movie year
        System.out.print("Enter the movie year ==> ");
        year = input.nextLine();
        //add movie in the arrayList
        movieList.add(new Movie(title, length, movingRating, catgName, year));
        //check if the category has a movie
        int index = retrieveCategory(catgName);
        if (index != -1) {
            //current size in category element
            int currentSize = categoryList.get(index).getSize();
            categoryList.get(index).setSize(currentSize + 1);
        }
        else {
            categoryList.add(new Category(catgName, 1));
        }
    }

    /**
     *
     */
    public void edit() {
        String[] features = {"TITLE", "LENGTH", "RATING", "CATEGORY, YEAR"};
        //declaring local variables
        String title, rating, catgName, length, year;
        //Get the movie index to edit by entering the its title
        int index;
        System.out.print("Enter the movie (title) to edit : ");
        title = input.nextLine();
        //check if the movie is in the list
        index = retrieveMovie(title);
        while (index == -1) {
            System.out.print("This movie does not exists / or enter the correct title : ");
            title = input.nextLine();
            index = retrieveMovie(title);
        }
        //
        System.out.println("Which feature you would like to edit? Title / Length / rating / Category / Year.");
        System.out.print("Enter feature to edit : ");
        String feature = input.nextLine().toUpperCase();
        //checking if input is valid
        while(checkElement(features, feature) == false) {
            System.out.println("Wrong input: enter a valid feature");
            feature = input.nextLine().toUpperCase();
        }
        System.out.println("");
        switch(feature) {
            case "TITLE":
                System.out.print("Enter new title :  ");
                title = input.nextLine();
                movieList.get(index).setTitle(title);
                break;
            case "LENGTH":
                System.out.print("Enter new length :  ");
                length = input.nextLine();
                movieList.get(index).setLength(length);
                break;
            case "RATING":
                String[] ratings = {"G", "PG", "PG-13", "R", "NC-17"};
                System.out.print("Enter new rating :  ");
                rating = input.nextLine();
                //
                //The movie rating
                System.out.println("G / PG / PG-13 / R / NC-17");
                System.out.print("Enter the new rating ==> ");
                rating = input.nextLine();
                rating = rating.toUpperCase();
                //checking if input is valid
                while(checkElement(ratings, rating) == false) {
                    System.out.println("Wrong input: enter a valid rating");
                    rating = input.nextLine();
                }
                //
                Rating movingRating;
                if (rating == "G")
                    movieList.get(index).setRating(Rating.General_Audiences);
                else if (rating == "PG")
                    movieList.get(index).setRating(Rating.Parental_control_Guidance);
                else if (rating == "PG-13")
                    movieList.get(index).setRating(Rating.Parents_Strongly_Cautioned);
                else if (rating == "R")
                    movieList.get(index).setRating(Rating.Restricted);
                else
                    movieList.get(index).setRating(Rating.Clearly_Adult);
                //
                break;
            case "CATEGORY":
                System.out.print("Enter new category :  ");
                catgName = input.nextLine();
                movieList.get(index).setCategory(catgName);
                break;
            default:
                System.out.print("Enter new year :  ");
                year = input.nextLine();
                movieList.get(index).setYear(year);
        }
    }

    /**
     * delete() function with no argument
     * the function delete a movie in the movie list
     * the function also delete a category in the category list
     */
    public void delete() {
        //The movie category
        String title;
        System.out.print("Which movie you would like to delete. ==> ");
        System.out.print("Enter the movie title. ==> ");
        title = input.nextLine();
        //check if the movie is in the list
        int index = retrieveMovie(title);
        if (index != -1) {
            movieList.remove(index);
            //current size in category element
            int currentSize = categoryList.get(index).getSize();
            categoryList.get(index).setSize(currentSize - 1);
            currentSize = categoryList.get(index).getSize();
            if (currentSize <= 0) {
                categoryList.remove(index);
            }
        }
        else {
            System.out.print("The movie does not exist. Or wrong movie spelling ");
        }

    }

    /**
     * displayMovies() function with no argument
     * the function displays the list of movies
     */
    public void displayMovies() {
        for (int i = 0; i < movieList.size(); i++) {
            System.out.println(movieList.get(i));
        }
    }

    /**
     * displayCategories() function with no argument
     * the function displays the list of category
     */
    public void displayCategories() {
        for (int i = 0; i < categoryList.size(); i++) {
            categoryList.get(i).toString();
        }
    }
    //************************************************************
    //*********** PRIVATE FUNCTIONS ******************************
    //************************************************************
    /**
     * @param arr
     * @param element
     * @return - return true or false
     * the function checkRating validate if the input corresponds to one of the ratings
     */
    private boolean checkElement(String[] arr, String element) {
        boolean isIn = false;
        for (String current : arr) {
            if (current.equals(element) == true) {
                isIn = true;
                break;
            }
        }
        return isIn;
    }

    /**
     * @param category - the category of type String
     * @return index - return a value of type integer
     * the function retrieveCategory returns the index of the input in the
     * category list. Otherwise the function returns -1; meaning the input
     * does not exist in the category list.
     */
    private int retrieveCategory(String category) {
        int index = -1;
        if (categoryList == null)
            return index;
        else if (categoryList.isEmpty())
            return index;
        for (int i = 0; i < categoryList.size(); i++) {
            if (categoryList.get(i).getName() == category) {
                index = i;
                break;
            }
        }
        return index;
    }

    /**
     * @param movie - the movie of type String
     * @return index - return a value of type integer
     * the function retrieveMovie returns the index of the input in the
     * movie list. Otherwise the function returns -1; meaning the input
     * does not exist in the movie list.
     */
    private int retrieveMovie(String title) {
        int index = -1;
        if (movieList == null)
            return index;
        else if (movieList.isEmpty())
            return index;
        for (int i = 0; i < movieList.size(); i++) {
            if (movieList.get(i).getTitle() == title) {
                index = i;
                break;
            }
        }
        return index;
    }
    //************************************************************
    //************************************************************
    //************************************************************
}
