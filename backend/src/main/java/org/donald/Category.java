package org.donald;

/**
 * @author DONALD KEITA
 *
 */

public class Category {
    //name::the name of the category
    private String name;
    //size::number of movies in a category
    private int size;

    /**
     * @param category
     * @param size
     */
    public Category(String name, int size) {
        this.name = name;
        this.size = size;
    }

    /**
     * @return the category
     */
    public String getName() {
        return name;
    }

    /**
     * @param name the name to set
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * @return the size
     */
    public int getSize() {
        return size;
    }

    /**
     * @param size the size to set
     */
    public void setSize(int size) {
        this.size = size;
    }

}
